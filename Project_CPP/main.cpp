﻿#include <iostream>

using namespace std;

class IA   //Class type unterface whithout variable  (prefics I)
{
public:
	IA()
	{
		cout << "Class A is Created" << endl;
	}
	virtual ~IA()  // Destroy auto class B (virtual)
	{
		cout << "Class A is Deleted" << endl;
	}

	virtual void Test() = 0;  // A - is Abstract class	
};

void IA::Test()
{
	cout << "default implementation" << endl;
}

class B : virtual public IA
{
public:
	B()
	{
		cout << "Create class B" << endl;
	}
	~B()
	{
		cout << "Class B is Deleted" << endl;
	}

	virtual void Test() override
	{
		IA::Test();
		cout << "Test from B" << endl;
	}
	
};

class C :virtual  public IA
{
public:
	C()
	{
		cout << "Class C is Created" << endl;
	}
	~C()
	{
		cout << "Class C is Deleted" << endl;
	}
	virtual void Test() override
	{
		IA::Test();
	}
};

class D : virtual public B, virtual public C
{
public:
	D()
	{
		cout << "Class D is Created" << endl;
	}
	~D()
	{
		cout << "Class D is Deleted" << endl;
	}
	virtual void Test() override
	{
		IA::Test();
	}
};

int main()
{
	
	IA* b = new B;
	b->Test();
	delete b;
	
	cout << "" << endl; cout << "" << endl; cout << "" << endl;

	D* d = new D;
	delete d;


	return 0;
}
	



